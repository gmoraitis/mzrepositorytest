<?php 
namespace MarzipanoTool;
error_reporting(E_ALL); 
ini_set('display_errors', 1);
if(!class_exists("MarzipanoApp")){
  class MarzipanoApp{
    
    private $levels = Array();
    private $name;
    private $path;
    private $debug;
    private $debugFile;
    
    private $validFaces =  Array("l","r","u","d","f","b");
    private $tileFactor = 2;

    private $scriptsURL = ""; //used for automanage
    
    function __construct(string $name , string $path , $debug = 0){
      $this->name = $name;
      $path = ltrim($path,"/");
      
      $this->assertFolder($path);
      $this->assertFolder($path."/tiles");

      $this->path = $path;
      $this->levels = Array();
      $this->addLevel();
      $this->addLevel();
    } 
      
    function addLevel(){
      $num = count($this->levels);
      $index = $num;
      $this->assertFolder($this->path."/tiles/$index");
      foreach($this->validFaces as $face){
        $this->assertFolder($this->path."/tiles/$index/$face");
        //x of each tile
        for($i = 0; $i < pow($this->tileFactor , $num ); $i++){
          $this->assertFolder($this->path."/tiles/$index/$face/".$i);
        }
      }
      $this->levels[] = Array();
    }
      
    function import($fileName){
        if(is_file($fileName)){
            $info = pathinfo($fileName);
            if($info && isset($info['extension']) && $info['extension']="zip"){
                
                $Zip = new \ZipArchive();
                if (!$Zip->open($fileName)) {
                    throw new Exception("Failed to open $fileName");
                }
                
                $exclusions = Array("app-files/index.js",
                    "app-files/style.css",
                    "LICENSE.txt",
                    "README.txt",
                    "vendor");
                
                for ($i = 0; $i < $Zip->numFiles; $i++) {
                    $valid = 1;
                    $name = $Zip->getNameIndex($i);
                    foreach ($exclusions as $ex) {
                        if (strpos($name, $ex) !== false) {
                            $valid = 0;
                            break;
                        }
                    }
                    if ($valid) {
                        umask(0);
                        if(!$Zip->extractTo($this->path, $name)){
                            throw new Exception("Failed to extract $name");
                        }
                    }
                }
          
                if(file_exists($this->path."/app-files/index.html") && isset($this->scriptsURL)){
                    
                    $str = file_get_contents($this->scriptsURL."/app-files/index.html");
                    $str = preg_replace('/<script src="vendor/','<script src="'.$this->scriptsURL.'/vendor',$str);
                    $str = str_replace('href="vendor/reset.min.css"','href="'.$this->scriptsURL.'/reset.min.css"',$str);
                    $str = str_replace('href="style.css"','href="'.$this->scriptsURL.'/style.css"',$str);
                    $str = str_replace('<script src="index.js','<script src="'.$this->scriptsURL.'/index.js',$str);
                    file_put_contents($this->path."/app-files/index.html",$str);   
                }
                
                return true;
            }
            else throw new \Exception("$fileName not a valid zip file");
        }
        else throw new \Exception("Attempting to import invalid / non-existing file");
    } 
    
    function assertFolder(string $path, $forceNew = 0){
      if(!file_exists($path)){
        umask(0);
        if(!mkdir($path,0777)){
          throw new \Exception("Error creating folder $path");
        }
      }
      else{
        if( $forceNew ){
          throw new \Exception("Error : given path already exists");
        }
      }
    }

    function gatherData(){
      $data = Array();
      $data['APP_NAME'] = $this->name;

      $levels = Array();
      foreach($this->levels as $l){
        if(isset($l['size'])){
          $levels[] = Array("size" => $l['size'] , "tileSize" => $l['size']);
        }
        else {
            break;
        }
      }
      $data['LEVELS_SIZE'] = array_reverse($levels);
      return $data;
    }
    
    function setScriptsURL(string $url){
      $this->scriptsURL = $url;
    }
    function autoManage(array $arr){
        if (!empty($arr) && count($arr > 0)) {
                $this->addLevel();
                foreach ($arr as $name => $file) {
                    if (in_array($name, $this->validFaces) &&
                            isset($file['tmp_name']) &&
                            isset($file['type']) &&
                            $file['type'] === 'image/png' &&
                            is_file($file['tmp_name'])) {
                        
                        $tile = new MarzipanoTile($file['tmp_name'], $name, $this);
                        $this->addTile($tile, 0, 0, 0);
                        
                        $parent = $tile;
                        for ($lev = 1; $lev < 2; $lev++) {
                            $splitArr = ImageHandler::split($parent->getImage(), pow(4, $lev));
                            foreach ($splitArr as $x => $yArr) {
                                foreach ($yArr as $y => $img) {
                                    $tile = new MarzipanoTile($img, $name, $this);
                                    $this->addTile($tile, $lev, $y, $x);
                                }
                            }
                        }
                    }
                }

                $data = $this->gatherData();

                $scriptBuilder = new ScriptBuilder();
                $scriptBuilder->
                        //Create index.js file
                        setTarget("./Prototypes/index.js")
                        ->load()
                        ->setTag("APP_NAME", $data["APP_NAME"])
                        ->setTag("LEVELS_SIZE", $data["LEVELS_SIZE"])
                        ->setTarget($this->path . "/index.js", "w")
                        ->save()
                        //Create index.html and inject includes
                        ->setBaseURL($this->scriptsURL)
                        ->includeDefaults()
                        ->includej("index.js")
                        ->setTarget("./Prototypes/index.html")
                        ->load()
                        ->setTag("INCLUDES_SCRIPTS", $scriptBuilder->formatIncludes()["js"])
                        ->setTarget($this->path . "/index.html", "w")
                        ->save()
                        //Create style.css
                        ->setTarget("./Prototypes/style.css")
                        ->load()
                        ->setTarget($this->path . "/style.css", "w")
                        ->save();
            }
        }

    function getValidFaces(){
      return $this->validFaces;
    }
    function getLevels(){
      return $this->levels;
    }
    function getName(){
      return $this->name;
    }
    function getPath(){
      return $this->path;
    }
    function addTile(MarzipanoTile $tile , int $level , int $x ,int $y){
      $face  = $tile->getFace();
      $limit = pow($this->tileFactor,$level);
      if($level >= 0 && $level < count($this->levels) && $x < $limit && $y < $limit) {
        ImageHandler::saveImage($tile->getImage(),$this->path."/tiles/".$level."/".$face."/$x/$y.jpg");
        if($level>0){
          imagedestroy($tile->getImage());
        }
        if(!isset($this->levels[$level][$tile->getFace()])){
          $this->levels[$level][$tile->getFace()] = Array();
        }
        if(!isset($this->levels[$level][$tile->getFace()][$x])){
          $this->levels[$level][$tile->getFace()][$x] = Array();
        }
        $this->levels[$level][$tile->getFace()][$x][$y] = $tile;

        $this->levels[$level]["size"] = $tile->getHeight();
       // ImageHandler::deleteImage($tile->getImage());
      }
      else{
          throw new \Exception("Attempt to assign tile with invalid parameters");
      }
    }
    
    
  }
}

if(!class_exists("MarzipanoTile")){
  class MarzipanoTile{
    
    private $width;
    private $height;
    private $face;
    private $image;
    
    function __construct($path , string $face , MarzipanoApp $app){
      
      if(!in_array($face,$app->getValidFaces())){
        throw new \Exception("Invalid face provided to tile");
      }
      if(is_resource($path) && get_resource_type($path)==="gd"){
      	$img = $path;
        $size = Array(imagesx($img),imagesy($img));
      }
      else if($img = ImageHandler::makeImage($path)){
        
        if(!$size = getimagesize($path)){
          throw new \Exception("Error : Could not create tile image");
        }

        if($size[0] <= 0 || $size[1] <= 0 || !($size[0]===$size[1])){
          throw new \Exception("Error : invalid image size provided");          
        }
               
        
      }
      $this->width  = $size[0];
      $this->height = $size[1];
      $this->face   = $face;
      $this->image  = $img;
    }

    function getFace(){
      return $this->face;
    }
    function getHeight(){
      return $this->height;
    }
    function getWidth(){
      return $this->width;
    }
    function getImage(){
      return $this->image;
    }
  }
}

if(!class_exists("ImageHandler")){
  class ImageHandler{
    
      public static function makeImage($path) {
            if (!is_file($path)) {
                throw new \Exception("Error : $path not a valid image");
            }
            if (!$img = imagecreatefrompng($path)) {
                if (!$img = imagecreatefromstring($path)) {
                    throw new \Exception("Error : invalid image provided");
                }
            }
            return $img;
        }

        public static function saveImage($image, $path) {
            imagejpeg($image, $path);
        }

        public static function deleteImage($image) {
            imagedestroy($image);
        }

        public static function split($image, int $numpieces) {
            if ($numpieces <= 0 || $numpieces % 2 !== 0) {
                throw new \Exception("Invalid number of tiles for image split");
            }

            $x = imagesx($image);
            $y = imagesy($image);
            if ($x && $y && ( $x == $y )) {
                $side = sqrt($numpieces);
                $s = $x / $side; //Width/height of square sub-tile


                $output = Array();
                for ($i = 0; $i < $side; $i++) {
                    $output[$i] = Array();
                    for ($j = 0; $j < $side; $j++) {
                        $newimg = imagecreatetruecolor($x, $x);
                        imagecopyresized($newimg, $image, 0, 0, $s * $i, $s * $j, $x, $x, $s, $s);

                        if (!$newimg) {
                            throw new \Exception("Error : failed to split image");
                        }
                        $output[$i][$j] = $newimg;
                    }
                }
                return $output;
            }
        }

    }

}


if(!class_exists("ScriptBuilder")){
  
  class ScriptBuilder{
    
    private $validFiles = Array("js","html","css");
    private $target;
    private $store;

    private $url;
    private $includes = Array();
    private $defaults = Array(
      "bowser.min.js",
      "classList.js",
      "es5-shim.js",
      "eventShim.js",
      "marzipano.js",
      "requestAnimationFrame.js",
      "screenfull.min.js"
    );

    function __construct(){ 
      foreach($this->validFiles as $ext){
        $this->includes[$ext] = Array();
      }
    }
    
    function setTarget($path , $mode = "a+"){
      

        $info = pathinfo($path);
        
        if(!in_array($info['extension'], $this->validFiles)){
          throw new \Exception("File extension is not valid");
        }
        
        $fp = fopen($path,$mode);

        if($this->target){
          fclose($this->target);
        }
          
        $this->target = $fp;     
      
      
      return $this;
    }    
    
    function setBaseURL(string $url){
      $this->url = htmlentities(trim($url,"/"));
      return $this;
    }
    function includej(string $script , $base = 0){
      $url = $base? $this->url."/" : "";
      $info = pathinfo($script);
      if($info && isset($info['extension']) && in_array($info['extension'],$this->validFiles)){
        $this->includes[$info['extension']][] = "$url"."$script";
      }
      return $this;
    }

    function includeDefaults(){
      foreach($this->defaults as $def){
        //$this->includes["js"][] = $this->url."/$def";
        $this->includej($def,1);
      }
      return $this;
    }

    function formatIncludes(){

      $formatted = Array();
      foreach($this->validFiles as $ext){
        $formatted[$ext] = "";
        switch($ext){
          case "js":
            foreach($this->includes["js"] as $inc){
              $formatted["js"] .= "<script src=\"$inc\"></script>".PHP_EOL;
            }

          //todo css
        }
      }
      return $formatted;
    }

    function load(){
        if($this->target){
            fseek($this->target,0);
            $this->store = stream_get_contents($this->target);
        }
        return $this;
    }

    function save(){
      if($this->target){
        fwrite($this->target,$this->store);
      }
      return $this;
    }

    function setTag($tagName , $replacement){
        if($this->store){
            $tag = "{%$tagName%}";
            if(is_array($replacement)){
              $replacement = json_encode($replacement,JSON_PRETTY_PRINT);
            }
            $this->store = str_replace($tag,$replacement,$this->store);
        }
        return $this;
    }

    function setAllTags(array $tags){
      if(!empty($tags) && $this->store){
        
        foreach($tags as $tag => $replacement){
          $this->setTag($tag,$replacement);
        }
        
      }  
      return $this;
    }

    function __destruct(){
      if($this->target){
        fclose($this->target);
      }
    }

  } 
}