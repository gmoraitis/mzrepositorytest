var panoElement = document.getElementById('pano');
var viewerOpts = {
  controls: {
    mouseViewMode: 'drag'    // drag|qtvr
  }
};

var viewer = new Marzipano.Viewer(panoElement, viewerOpts);

var levels = {%LEVELS_SIZE%} ;
  //{ tileSize: 1024, size: 1024 },
  

var geometry = new Marzipano.CubeGeometry(levels);

var source = Marzipano.ImageUrlSource.fromString("tiles/{z}/{f}/{y}/{x}.jpg", {
  
});

var initialView = {
  yaw: 90 * Math.PI/180,
  pitch: -30 * Math.PI/180,
  fov: 90 * Math.PI/180
};

var limiter = Marzipano.RectilinearView.limit.traditional(1024, 120*Math.PI/180);

var view = new Marzipano.RectilinearView(initialView, limiter);

var scene = viewer.createScene({
  source: source,
  geometry: geometry,
  view: view,
  pinFirstLevel: true
});
scene.switchTo({
  transitionDuration: 1000
});